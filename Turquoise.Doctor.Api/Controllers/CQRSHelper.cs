﻿using System;
using MediatR;
using System.Threading.Tasks;

namespace Turquoise.Doctor.Api.Controllers
{
    public class CQRSHelper : Controller
    {
        private readonly IMediator mediator;
        public CQRSHelper(IMediator mediator) =>
            this.mediator = mediator ?? throw new ArgumentNullException(
                  nameof(IMediator)
                );

        /// <summary>
        /// Execute a query
        /// </summary>
        /// <typeparam name="T">Response type</typeparam>
        /// <param name="query">Query object</param>
        /// <returns></returns>
        public virtual async Task<T> PublishCQAsync<T>(IRequest<T> query) => await mediator.Send(query);
    }
}
