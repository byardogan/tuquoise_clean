﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Turquoise.Doctor.Api.Controllers
{
    using Turquoise.Doctor.Api.Aspects;
    using Turquoise.Doctor.Domain.DomainModel;

    /// <summary>
    /// Base Controller
    /// </summary>
    [
        ApiController,
        TypeFilter(typeof(ExceptionHandlerAttribute))
    ]
    public class Controller : ControllerBase
    {
        /// <summary>
        /// Single object result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        protected virtual ActionResult<Single<T>> Single<T>(T data) => Ok(
                new Single<T>(data)
                );

        /// <summary>
        /// Multiple object result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        protected virtual ActionResult<Multiple<T>> Multiple<T>(IEnumerable<T> data) => Ok(
                new Multiple<T>(data)
                );

        /// <summary>
        /// Command result
        /// </summary>
        /// <returns></returns>
        protected virtual ActionResult<Result> Result(MediatR.Unit unit) => Ok(
                new Result(Status.Done)
                );
    }
}
