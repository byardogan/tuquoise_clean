﻿using System;
using MediatR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Turquoise.Doctor.Api.Controllers
{
    using Turquoise.Doctor.Api.Aspects;
    using Turquoise.Doctor.Domain.Aggregates;
    using Turquoise.Doctor.Domain.DomainModel;

    /// <summary>
    /// CQRS controller
    /// </summary>
    /// <typeparam name="TViewModel"></typeparam>
    [
        ModelValidationAspect
    ]
    public class Crud<TViewModel> : Query<TViewModel> where TViewModel : ViewModel
    {
        public Crud(IMediator mediator) : base(mediator)
        {
        }

        /// <summary>
        /// It removes a single record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [
            HttpDelete("{id}"),
            ProducesResponseType(200),
            ProducesResponseType(404),
            ProducesResponseType(401),
        ]
        public virtual async Task<ActionResult<Result>> Delete(Guid id)
        {
            var command = new Commands.Delete<TViewModel>(id);
            return Result(await PublishCQAsync(command));
        }

        /// <summary>
        /// It creates a new record
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [
            HttpPost,
            ProducesResponseType(200),
            ProducesResponseType(400),
            ProducesResponseType(401),
        ]
        public virtual async Task<ActionResult<Result>> Create([FromBody] TViewModel viewModel)
        {
            var command = new Commands.Insert<TViewModel>(viewModel);
            return Result(await PublishCQAsync(command));
        }

        /// <summary>
        /// It updates an existings record
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [
            HttpPut,
            ProducesResponseType(200),
            ProducesResponseType(400),
            ProducesResponseType(401),
        ]
        public virtual async Task<ActionResult<Result>> Update([FromBody] TViewModel viewModel)
        {
            var command = new Commands.Update<TViewModel>(viewModel);
            return Result(await PublishCQAsync(command));
        }
    }
}
