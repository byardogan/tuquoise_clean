﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Turquoise.Doctor.Api.Controllers.Users
{
    using Turquoise.Doctor.Domain.Aggregates.DoctorGroup;

    [Route("api/[controller]")]
    public class DoctorGroupsController : Crud<DoctorGroupViewModel>
    {
        public DoctorGroupsController(IMediator mediator) : base(mediator)
        {
        }
    }
}
