﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Turquoise.Doctor.Api.Controllers.Users
{
    using Turquoise.Doctor.Domain.Aggregates.User;

    [Route("api/[controller]")]
    public class UsersController : Crud<UserViewModel>
    {
        public UsersController(IMediator mediator) : base(mediator)
        {
        }
    }
}
