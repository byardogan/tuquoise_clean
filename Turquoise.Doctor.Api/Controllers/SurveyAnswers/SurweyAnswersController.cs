﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Turquoise.Doctor.Domain.Aggregates.SurveyAnswer;

namespace Turquoise.Doctor.Api.Controllers.SurveyAnswers
{
    [Route("api/[controller]")]
    public class SurweyAnswersController : Crud<SurveyAnswerViewModel>
    {
        public SurweyAnswersController(IMediator mediator) : base(mediator)
        {
        }
    }
}
