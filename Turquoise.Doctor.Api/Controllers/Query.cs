﻿using System;
using MediatR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Turquoise.Doctor.Api.Controllers
{
    using Turquoise.Doctor.Domain.Aggregates;
    using Turquoise.Doctor.Domain.DomainModel;

    public class Query<TViewModel> : CQRSHelper where TViewModel : ViewModel
    {
        public Query(IMediator mediator) : base(mediator)
        {
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [
            HttpGet("{id}"),
            ProducesResponseType(200),
            ProducesResponseType(404),
            ProducesResponseType(401),
        ]
        public virtual async Task<ActionResult<Single<TViewModel>>> Get(Guid id)
        {
            var query = new Queries.Get<TViewModel>(id);
            return Single(await PublishCQAsync(query));
        }

        /// <summary>
        /// Get by page 
        /// </summary>
        /// <param name="pagination"></param>
        /// <returns></returns>
        [
            HttpPut("page"),
            ProducesResponseType(200),
            ProducesResponseType(404),
            ProducesResponseType(401),
        ]
        public virtual async Task<ActionResult<Multiple<TViewModel>>> GetPage([FromBody] Pagination pagination)
        {
            var query = new Queries.GetPage<TViewModel>(pagination);
            return Multiple(await PublishCQAsync(query));
        }
    }
}

