﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Turquoise.Doctor.Api.Controllers.Surveys
{
    using Turquoise.Doctor.Domain.Aggregates.Survey;

    /// <summary>
    /// Surveys
    /// </summary>
    [
        Route("api/[controller]")
    ]
    public class SurveysController : Crud<SurveyViewModel>
    {
        public SurveysController(IMediator mediator) : base(mediator)
        {
        }
    }
}
