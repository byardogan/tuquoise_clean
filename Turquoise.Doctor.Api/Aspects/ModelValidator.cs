﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Turquoise.Doctor.Api.Aspects
{
    using Turquoise.Doctor.Domain.DomainModel;

    /// <summary>
    /// Model validation 
    /// </summary>
    [
        AttributeUsage(
            AttributeTargets.Class | AttributeTargets.Method,
            Inherited = true,
            AllowMultiple = false)
    ]
    public class ModelValidationAspectAttribute : ResultFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                string[] errors = context.ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage)
                    .ToArray();

                context.Result = new BadRequestObjectResult(new Fail(errors));
            }
            base.OnResultExecuting(context);
        }
    }
}
