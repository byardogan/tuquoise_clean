﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Turquoise.Doctor.Api.Aspects
{
    using Turquoise.Doctor.Domain.DomainModel;

    /// <summary>
    /// 
    /// </summary>
    [
        AttributeUsage(
            AttributeTargets.Class | AttributeTargets.Method,
            Inherited = true,
            AllowMultiple = false)
    ]
    public class ExceptionHandlerAttribute : ExceptionFilterAttribute
    {
        public ExceptionHandlerAttribute()
        {
        }

        /// <summary>
        /// Handle exception
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async override Task OnExceptionAsync(ExceptionContext context)
        {
            //get exception stack trace
            static IEnumerable<string> GetStackTrace(Exception exception)
            {
                Exception next = exception;
                do
                {
                    yield return next.Message;
                    next = next.InnerException;
                }
                while (!(next is null));
            }

            string[] errors = GetStackTrace(context.Exception).ToArray();
            context.Result = new BadRequestObjectResult(new Fail(errors));
            await base.OnExceptionAsync(context);
        }
    }
}
