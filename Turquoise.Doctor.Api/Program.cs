using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Turquoise.Doctor.Api
{
    public class Program
    {
        /// <summary>
        /// Start
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args) => 
            CreateHostBuilder(args)
            .Build()
            .Run();

        /// <summary>
        /// Build host
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>());
    }
}
