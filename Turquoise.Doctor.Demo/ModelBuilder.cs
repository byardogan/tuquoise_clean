﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using Turquoise.Doctor.Demo.Models;

namespace Turquoise.Doctor.Demo
{
    public class ModelBuilder
    {
        private static readonly HttpClientContext httpClientContext =
            new HttpClientContext();

        public static async Task<T> Get<T>(string path)
        {
            string json = await httpClientContext.Get(path);
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static async Task<T> Put<T>(string path)
        {
            string json = await httpClientContext.Put(path);
            return JsonConvert.DeserializeObject<T>(json);
        }

        public async static Task<PostResult> Post<T>(string path, T body)
        {
            string response = await httpClientContext.Post(path, JsonConvert.SerializeObject(body));
            return JsonConvert.DeserializeObject<PostResult>(response);
        }
    }
}
