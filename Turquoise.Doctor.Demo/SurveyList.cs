﻿using System;
using System.Linq;
using System.Windows.Forms;
using Turquoise.Doctor.Demo.Models;

namespace Turquoise.Doctor.Demo
{
    public partial class SurveyList : Form
    {
        public SurveyList()
        {
            InitializeComponent();
        }

        async void BtnViewSurvey_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.SelectedRows.Count == 0)
                {
                    return;
                }

                var id = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
                Data<SurveyViewModel> surveyViewModel = await ModelBuilder.Get<Data<SurveyViewModel>>
                       ($"/api/surveys/{id}");

                new ViewSurvey(surveyViewModel.Response).ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        async void SurveyList_Load(object sender, EventArgs e)
        {
            Multiple<SurveyViewModel> surveyViewModel = await ModelBuilder.Put<Multiple<SurveyViewModel>>("/api/surveys/page");

            var surveys = surveyViewModel.Data.ToList();
            surveys.ForEach(x => x.Image = null);
            dataGridView1.DataSource = surveys;
        }
    }
}
