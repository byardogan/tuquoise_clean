﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;
using Turquoise.Doctor.Demo.Models;

namespace Turquoise.Doctor.Demo
{
    public partial class ViewSurvey : Form
    {
        Point lastPoint = Point.Empty;
        bool isMouseDown = new bool();

        public ViewSurvey(SurveyViewModel surveyViewModel)
        {
            InitializeComponent();

            foreach (var item in surveyViewModel.Choices)
            {
                RadioButton radio = new RadioButton()
                {
                    Text = item.Option,
                    Tag = item.Id
                };
                flowChoices.Controls.Add(radio);
            }

            if (surveyViewModel.Image != null)
            {
                Image image = null;
                using (MemoryStream ms = new MemoryStream(surveyViewModel.Image))
                {
                    image = Image.FromStream(ms);
                    panel1.BackgroundImage = image;
                }

                coordinateHelper = new CoordinateHelper()
                {
                    ContainerHeight = pbImage.Height,
                    ContainerWidth = pbImage.Width,
                    OriginalHeight = image.Height,
                    OriginalWidth = image.Width
                };
            }
        }

        private void ViewSurvey_Load(object sender, EventArgs e)
        {

        }

        void PnlColor_Click(object sender, EventArgs e)
        {
            ColorDialog dialog = new ColorDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                PnlColor.BackColor = dialog.Color;
            }
        }

        private CoordinateHelper coordinateHelper;
        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            if (pbImage.Image != null)
            {
                coordinateHelper.Clear();
                pbImage.Image = null;
                Invalidate();
            }
        }

        async void BtnSend_Click(object sender, EventArgs e)
        {
            Guid? choice = null;
            foreach (var item in flowChoices.Controls)
            {
                if (item is RadioButton radioButton)
                {
                    if (radioButton.Checked)
                    {
                        choice = (Guid?)radioButton.Tag;
                        break;
                    }
                }
            }

            var model = new SurveyAnswerViewModel()
            {
                Comments = txtComment.Text,
                ChoiceId = choice.GetValueOrDefault(),
                Coordinates = coordinateHelper.GetCorrdinateSummary()
            };

            PostResult status = await ModelBuilder.Post("api/SurweyAnswers", model);
            if (status.Status == (int)Status.Done)
            {
                MessageBox.Show("Kayıt yapıldı!");
                btnRefresh.PerformClick();
                txtComment.Text = string.Empty;
            }
        }

        private void PbImage_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = e.Location;
            isMouseDown = true;
        }

        private void PbImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown == true)
            {
                if (lastPoint != null)
                {

                    if (pbImage.Image == null)
                    {
                        Bitmap bmp = new Bitmap(pbImage.Width, pbImage.Height);
                        pbImage.Image = bmp;
                    }

                    using (Graphics g = Graphics.FromImage(pbImage.Image))
                    {
                        coordinateHelper.Add(e.Location.X, e.Location.Y);
                        g.DrawLine(new Pen(PnlColor.BackColor, (int)numericPenSize.Value), lastPoint, e.Location);
                        g.SmoothingMode = SmoothingMode.AntiAlias;
                    }

                    pbImage.Invalidate();
                    lastPoint = e.Location;
                }
            }
        }

        private void PbImage_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
            lastPoint = Point.Empty;
        }

        private void ViewSurvey_Resize(object sender, EventArgs e)
        {
            coordinateHelper.ContainerHeight = pbImage.Height;
            coordinateHelper.ContainerWidth = pbImage.Width;
        }
    }
}
