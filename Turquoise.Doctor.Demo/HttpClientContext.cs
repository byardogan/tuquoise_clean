﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Turquoise.Doctor.Demo.Models;

namespace Turquoise.Doctor.Demo
{
    public class HttpClientContext
    {
        private readonly HttpClient httpClient;
        public HttpClientContext() => httpClient = new HttpClient
        {
            BaseAddress = new System.Uri("https://localhost:44398")
        };

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="uri"></param>
        public async Task<string> Get(string uri)
        {
            var responseMessage = await httpClient.GetAsync(uri);
            return await responseMessage.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// Put
        /// </summary>
        /// <param name="uri"></param>
        public async Task<string> Put(string uri)
        {
            var page = new Pagination(1, int.MaxValue);
            var content = new StringContent(
                JsonConvert.SerializeObject(page),
                Encoding.UTF8,
                "application/json");

            var responseMessage = await httpClient.PutAsync(uri, content);
            return await responseMessage.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// Post
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public async Task<string> Post(string uri, string json)
        {
            var content = new StringContent(
                json,
                Encoding.UTF8,
                "application/json");
            var responseMessage = await httpClient.PostAsync(uri, content);
            return await responseMessage.Content.ReadAsStringAsync();
        }
    }
}
