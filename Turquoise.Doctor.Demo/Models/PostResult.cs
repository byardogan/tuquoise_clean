﻿using Newtonsoft.Json;

namespace Turquoise.Doctor.Demo.Models
{
    public class PostResult
    {
        [JsonProperty("status")]
        public int Status { get; set; }

    }
}
