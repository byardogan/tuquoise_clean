﻿using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Turquoise.Doctor.Demo.Models
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class Multiple<T> : Result
    {
        public Multiple() : base(Status.Done)
        {
        }

        [JsonProperty("rows")]
        public int Rows { get; set; }

        [JsonProperty("data")]
        public ICollection<T> Data { get; set; }
    }
}
