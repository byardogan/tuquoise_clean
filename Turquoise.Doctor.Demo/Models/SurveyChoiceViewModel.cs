﻿namespace Turquoise.Doctor.Demo.Models
{
    public class SurveyChoiceViewModel : EntityViewModel
    {
        public string Option { get; set; }
        public bool? Accepted { get; set; }
    }
}
