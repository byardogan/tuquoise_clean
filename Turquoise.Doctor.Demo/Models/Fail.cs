﻿using System.Collections.Generic;
using System.Linq;

namespace Turquoise.Doctor.Demo.Models
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class Fail : Result
    {
        public Fail() : base(Status.Failed)
        {
        }

        public Fail(IEnumerable<dynamic> errors) : this()
        {
            Errors = errors;
            Rows = errors.Count();
        }

        public int Rows { get; }
        public IEnumerable<dynamic> Errors { get; }
    }
}
