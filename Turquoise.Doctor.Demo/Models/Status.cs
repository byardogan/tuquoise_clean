﻿using System.ComponentModel;

namespace Turquoise.Doctor.Demo.Models
{
    /// <summary>
    /// 
    /// </summary>
    public enum Status : byte
    {
        [Description("Done")] Done,
        [Description("Failed")] Failed
    }
}
