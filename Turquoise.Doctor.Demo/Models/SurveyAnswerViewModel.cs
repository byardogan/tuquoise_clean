﻿using Newtonsoft.Json;
using System;

namespace Turquoise.Doctor.Demo.Models
{
    public class SurveyAnswerViewModel : EntityViewModel
    {
        [JsonProperty("choiceId")]
        public Guid ChoiceId { get; set; }

        [JsonProperty("comments")]
        public string Comments { get; set; }

        [JsonProperty("coordinates")]
        public string Coordinates { get; set; }
    }
}
