﻿namespace Turquoise.Doctor.Demo.Models
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class Single<T> : Result
    {
        public Single() : base(Status.Done)
        {
        }

        public Single(T data) : this() => Data = data;

        public T Data { get; }
    }
}
