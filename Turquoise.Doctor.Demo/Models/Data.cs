﻿
using Newtonsoft.Json;

namespace Turquoise.Doctor.Demo.Models
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TResponse"></typeparam>
    public sealed class Data<TResponse> : Result
    {
        [JsonProperty("data")]
        public TResponse Response { get; }

        public Data(TResponse data) : base(Status.Done) => Response = data;
    }
}
