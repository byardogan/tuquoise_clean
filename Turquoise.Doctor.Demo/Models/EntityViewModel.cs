﻿using System;

namespace Turquoise.Doctor.Demo.Models
{
    /// <summary>
    /// Base table Dto
    /// </summary>
    public abstract class EntityViewModel : ViewModel
    {
        public virtual Guid? Id { get; set; }
    }
}
