﻿
using Newtonsoft.Json;

namespace Turquoise.Doctor.Demo.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Result
    {
        private readonly Status status;
        public Result(Status status) => this.status = status;

        /// <summary>
        /// Status code
        /// </summary>
        [
            JsonProperty("status")
        ]
        public byte StatusCode { get => (byte)status; }
    }
}
