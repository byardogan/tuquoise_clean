﻿using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Turquoise.Doctor.Demo
{
    public class CoordinateHelper
    {
        public int ContainerWidth { get; set; }
        public int ContainerHeight { get; set; }
        public int OriginalWidth { get; set; }
        public int OriginalHeight { get; set; }

        private readonly HashSet<int> coordinatesX;
        private readonly HashSet<int> coordinatesY;
        public CoordinateHelper() => coordinatesX = coordinatesY = new HashSet<int>();

        public void Clear()
        {
            coordinatesX.Clear();
            coordinatesY.Clear();
        }

        public void Add(int x, int y)
        {
            var coordinate1 = OriginalWidth * x / ContainerWidth;
            var coordinate2 = OriginalHeight * y / ContainerHeight;

            coordinatesX.Add(coordinate1);
            coordinatesY.Add(coordinate2);
        }

        public string GetCorrdinateSummary()
        {
            var builder = new StringBuilder();
            for (int i = 0; i < coordinatesX.Count; i++)
            {
                builder.Append($"{coordinatesX.ElementAt(i)} {coordinatesY.ElementAt(i)}:");
            }
            return builder.ToString().Trim(':'); 
        }
    }
}
