﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Turquoise.Doctor.Application.UseCases.SurveyAnswers
{
    using Turquoise.Doctor.Domain.Abstraction;
    using Turquoise.Doctor.Domain.Aggregates;
    using Turquoise.Doctor.Domain.Aggregates.SurveyAnswer;

    public class SurveyAnswerCommandHandler : ReqeustHandler<ISurveyAnswerService>,
         IRequestHandler<Commands.Insert<SurveyAnswerViewModel>>,
         IRequestHandler<Commands.Update<SurveyAnswerViewModel>>,
         IRequestHandler<Commands.Delete<SurveyAnswerViewModel>>
    {
        public SurveyAnswerCommandHandler(IUnitOfWork persistence) : base(persistence)
        {
        }

        public async Task<Unit> Handle(Commands.Insert<SurveyAnswerViewModel> request, CancellationToken cancellationToken)
        {
            Service.Insert(request.ViewModel.Cast<SurveyAnswer>());
            await UnitOfWork.SaveChangesAsync();
            return Success();
        }

        public async Task<Unit> Handle(Commands.Update<SurveyAnswerViewModel> request, CancellationToken cancellationToken)
        {
            Service.Update(request.ViewModel.Cast<SurveyAnswer>());
            await UnitOfWork.SaveChangesAsync();
            return Success();
        }

        public async Task<Unit> Handle(Commands.Delete<SurveyAnswerViewModel> request, CancellationToken cancellationToken)
        {
            Service.Delete(request.Identity);
            await UnitOfWork.SaveChangesAsync();
            return Success();
        }
    }
}
