﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Turquoise.Doctor.Application.UseCases.DoctorGroups
{
    using Turquoise.Doctor.Domain.Aggregates;
    using Turquoise.Doctor.Domain.Aggregates.DoctorGroup;

    public partial class DoctorGroupRequestHandler : ReqeustHandler<IDoctorGroupService>,
        IRequestHandler<Queries.Get<DoctorGroupViewModel>, DoctorGroupViewModel>,
        IRequestHandler<Queries.GetPage<DoctorGroupViewModel>, DoctorGroupViewModel[]>
    {
        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<DoctorGroupViewModel> Handle(Queries.Get<DoctorGroupViewModel> request, CancellationToken cancellationToken)
        {
            DoctorGroupViewModel group = 
                Service.GetById(request.Identity)
                .Cast<DoctorGroupViewModel>();

            return SuccessWith(group);
        }

        /// <summary>
        /// Get by page
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<DoctorGroupViewModel[]> Handle(Queries.GetPage<DoctorGroupViewModel> request, CancellationToken cancellationToken)
        {
            DoctorGroupViewModel[] groups = 
                Service.GetByPage(request.Pagination)
                .Cast<DoctorGroupViewModel>();

            return SuccessWith(groups);
        }
    }
}
