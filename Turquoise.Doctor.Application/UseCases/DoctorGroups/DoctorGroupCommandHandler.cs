﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Turquoise.Doctor.Application.UseCases.DoctorGroups
{
    using Turquoise.Doctor.Domain.Aggregates;
    using Turquoise.Doctor.Domain.Abstraction;
    using Turquoise.Doctor.Domain.Aggregates.DoctorGroup;

    public partial class DoctorGroupRequestHandler : ReqeustHandler<IDoctorGroupService>,
        IRequestHandler<Commands.Insert<DoctorGroupViewModel>>,
        IRequestHandler<Commands.Update<DoctorGroupViewModel>>,
        IRequestHandler<Commands.Delete<DoctorGroupViewModel>>
    {
        public DoctorGroupRequestHandler(IUnitOfWork persistence) : base(persistence)
        {
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(Commands.Insert<DoctorGroupViewModel> request, CancellationToken cancellationToken)
        {
            Service.Insert(request.ViewModel.Cast<DoctorGroup>());
            await UnitOfWork.SaveChangesAsync();
            return Success();
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(Commands.Update<DoctorGroupViewModel> request, CancellationToken cancellationToken)
        {
            Service.Update(request.ViewModel.Cast<DoctorGroup>());
            await UnitOfWork.SaveChangesAsync();
            return Success();
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(Commands.Delete<DoctorGroupViewModel> request, CancellationToken cancellationToken)
        {
            Service.Delete(request.Identity);
            await UnitOfWork.SaveChangesAsync();
            return Success();
        }
    }
}
