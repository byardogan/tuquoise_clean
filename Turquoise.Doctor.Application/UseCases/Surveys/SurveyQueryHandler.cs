﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Turquoise.Doctor.Application.UseCases.Surveys
{
    using Turquoise.Doctor.Domain.Aggregates;
    using Turquoise.Doctor.Domain.Aggregates.Survey;

    public partial class SurveyRequestHandler : ReqeustHandler<ISurveyService>,
        IRequestHandler<Queries.Get<SurveyViewModel>, SurveyViewModel>,
        IRequestHandler<Queries.GetPage<SurveyViewModel>, SurveyViewModel[]>
    {
        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<SurveyViewModel> Handle(Queries.Get<SurveyViewModel> request, CancellationToken cancellationToken)
        {
            SurveyViewModel survey =
                 Service.GetById(request.Identity)
                 .Cast<SurveyViewModel>();
            return SuccessWith(survey);
        }

        /// <summary>
        /// Get by page
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<SurveyViewModel[]> Handle(Queries.GetPage<SurveyViewModel> request, CancellationToken cancellationToken)
        {
            SurveyViewModel[] surveys =
                  Service.GetByPage(request.Pagination)
                  .Cast<SurveyViewModel>();
            return SuccessWith(surveys);
        }
    }
}
