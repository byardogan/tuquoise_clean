﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Turquoise.Doctor.Application.UseCases.Surveys
{
    using Turquoise.Doctor.Domain.Abstraction;
    using Turquoise.Doctor.Domain.Aggregates;
    using Turquoise.Doctor.Domain.Aggregates.Survey;

    public partial class SurveyRequestHandler : ReqeustHandler<ISurveyService>,
        IRequestHandler<Commands.Insert<SurveyViewModel>>,
        IRequestHandler<Commands.Update<SurveyViewModel>>,
        IRequestHandler<Commands.Delete<SurveyViewModel>>
    {
        public SurveyRequestHandler(IUnitOfWork persistence) : base(persistence)
        {
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(Commands.Insert<SurveyViewModel> request, CancellationToken cancellationToken)
        {
            Service.Insert(request.ViewModel.Cast<Survey>());
            await UnitOfWork.SaveChangesAsync();
            return Success();
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(Commands.Update<SurveyViewModel> request, CancellationToken cancellationToken)
        {
            Service.Update(request.ViewModel.Cast<Survey>());
            await UnitOfWork.SaveChangesAsync();
            return Success();
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(Commands.Delete<SurveyViewModel> request, CancellationToken cancellationToken)
        {
            Service.Delete(request.Identity);
            await UnitOfWork.SaveChangesAsync();
            return Success();
        }
    }
}
