﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Turquoise.Doctor.Application.UseCases.Users
{
    using Turquoise.Doctor.Domain.Aggregates;
    using Turquoise.Doctor.Domain.Aggregates.User;

    public partial class UserRequestHandler : ReqeustHandler<IUserService>,
        IRequestHandler<Queries.Get<UserViewModel>, UserViewModel>,
        IRequestHandler<Queries.GetPage<UserViewModel>, UserViewModel[]>
    {
        /// <summary>
        /// Get a page
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<UserViewModel[]> Handle(Queries.GetPage<UserViewModel> request, CancellationToken cancellationToken)
        {
            UserViewModel[] users =
                Service.GetByPage(request.Pagination)
                 .Cast<UserViewModel>();
            return SuccessWith(users);
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<UserViewModel> Handle(Queries.Get<UserViewModel> request, CancellationToken cancellationToken)
        {
            UserViewModel user =
                Service.GetById(request.Identity)
                .Cast<UserViewModel>();
            return SuccessWith(user);
        }
    }
}
