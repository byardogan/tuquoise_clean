﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Turquoise.Doctor.Application.UseCases.Users
{
    using Turquoise.Doctor.Domain.Aggregates;
    using Turquoise.Doctor.Domain.Abstraction;
    using Turquoise.Doctor.Domain.Aggregates.User;

    public partial class UserRequestHandler : ReqeustHandler<IUserService>,
        IRequestHandler<Commands.Insert<UserViewModel>>,
        IRequestHandler<Commands.Update<UserViewModel>>,
        IRequestHandler<Commands.Delete<UserViewModel>>
    {
        public UserRequestHandler(IUnitOfWork persistence) : base(persistence)
        {
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(Commands.Update<UserViewModel> request, CancellationToken cancellationToken)
        {
            Service.Update(request.ViewModel.Cast<User>());
            await UnitOfWork.SaveChangesAsync();
            return Success();
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(Commands.Insert<UserViewModel> request, CancellationToken cancellationToken)
        {
            Service.Insert(request.ViewModel.Cast<User>());
            await UnitOfWork.SaveChangesAsync();
            return Success();
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(Commands.Delete<UserViewModel> request, CancellationToken cancellationToken)
        {
            Service.Delete(request.Identity);
            await UnitOfWork.SaveChangesAsync();
            return Success();
        }
    }
}
