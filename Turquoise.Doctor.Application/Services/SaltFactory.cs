﻿using System.Security.Cryptography;

namespace Turquoise.Doctor.Application.Services
{
    using Turquoise.Doctor.Domain.Abstraction;

    public class SaltFactory : ISaltFactory
    {
        public byte[] Generate()
        {
            const int SIZE = 24;
            var rngcsp = new RNGCryptoServiceProvider();

            byte[] buffer = new byte[SIZE];
            rngcsp.GetBytes(buffer);
            return buffer;
        }
    }
}
