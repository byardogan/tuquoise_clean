﻿using System.Security.Cryptography;

namespace Turquoise.Doctor.Application.Services
{
    using Turquoise.Doctor.Domain.Abstraction;

    public class HashProvider : IHashProvider
    {
        public byte[] Compute(string input, byte[] salt)
        {
            const int SIZE = 24;
            const int ITER = 10000;

            var rfc2898 = new Rfc2898DeriveBytes(input, salt, ITER); //pbkdf2
            return rfc2898.GetBytes(SIZE);
        }
    }
}
