﻿using System;

namespace Turquoise.Doctor.Application.Services
{
    using Turquoise.Doctor.Domain.Abstraction;

    public class IdentityService : IIdentity
    {
        /// <summary>
        /// Get active user
        /// </summary>
        public Guid User => Guid.Parse("9A25FF11-2453-4F04-9911-278B647F3A59");
    }
}
