﻿using MediatR;
using System.Threading.Tasks;

namespace Turquoise.Doctor.Application
{
    using Turquoise.Doctor.Domain;
    using Turquoise.Doctor.Domain.Aggregates;
    using Turquoise.Doctor.Domain.Abstraction;

    public abstract class ReqeustHandler<TRoot> where TRoot : IRoot
    {
        protected ReqeustHandler(IUnitOfWork persistence)
        {
            UnitOfWork = persistence;
            Service = persistence.GetService<TRoot>();
        }

        /// <summary>
        /// Current service
        /// </summary>
        public TRoot Service { get; }

        /// <summary>
        /// Unit of work
        /// </summary>
        public IUnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Identity service
        /// </summary>
        public IIdentity Identity => Dependency.As<IIdentity>();

        /// <summary>
        /// Completed command
        /// </summary>
        /// <returns></returns>
        public static Unit Success() => Unit.Value;

        /// <summary>
        /// Completed command
        /// </summary>
        /// <returns></returns>
        public static Task<TResult> SuccessWith<TResult>(TResult with) => Task.FromResult(with);
    }
}
