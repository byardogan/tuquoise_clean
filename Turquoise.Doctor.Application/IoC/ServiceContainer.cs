﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Turquoise.Doctor.Application.Ioc
{
    using Turquoise.Doctor.Domain;
    using Turquoise.Doctor.Domain.Abstraction;
    using Turquoise.Doctor.Application.Services;

    /// <summary>
    /// Dependency injection container
    /// </summary>
    public partial class ServiceActivator
    {
        public void Register(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Dependency).Assembly); //automapper
            services.AddSingleton<IHashProvider, HashProvider>(); //password hash provider
            services.AddSingleton<ISaltFactory, SaltFactory>(); //password salt generator
            services.AddSingleton<IIdentity, IdentityService>(); //user service
        }
    }
}
