﻿using System.Text.Json.Serialization;

namespace Turquoise.Doctor.Domain.DomainModel
{
    /// <summary>
    /// 
    /// </summary>
    public class Result
    {
        private readonly Status status;
        public Result(Status status) => this.status = status;

        /// <summary>
        /// Status code
        /// </summary>
        [
            JsonPropertyName("status")
        ]
        public byte StatusCode { get => (byte)status; }
    }
}
