﻿using System.Linq;
using System.Collections.Generic;

namespace Turquoise.Doctor.Domain.DomainModel
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class Multiple<T> : Result
    {
        public Multiple() : base(Status.Done)
        {
        }

        public Multiple(IEnumerable<T> data) : this()
        {
            Data = data;
            Rows = data.Count();
        }

        public int Rows { get; }
        public IEnumerable<T> Data { get; }
    }
}
