﻿using System.Text.Json.Serialization;

namespace Turquoise.Doctor.Domain.DomainModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TResponse"></typeparam>
    public sealed class Data<TResponse> : Result
    {
        [JsonPropertyName("data")]
        public TResponse Response { get; }

        public Data(TResponse data) : base(Status.Done) => Response = data;
    }
}
