﻿using System.ComponentModel;

namespace Turquoise.Doctor.Domain.DomainModel
{
    /// <summary>
    /// 
    /// </summary>
    public enum Status : byte
    {
        [Description("Done")] Done,
        [Description("Failed")] Failed
    }
}
