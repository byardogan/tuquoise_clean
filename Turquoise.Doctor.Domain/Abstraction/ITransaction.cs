﻿using System;
using System.Threading.Tasks;

namespace Turquoise.Doctor.Domain.Abstraction
{
    /// <summary>
    /// Database transaction
    /// </summary>
    public interface ITransaction : IDisposable
    {
        /// <summary>
        /// Cancell changes
        /// </summary>
        /// <returns></returns>
        Task RollbackAsync();

        /// <summary>
        /// Accept changes
        /// </summary>
        /// <returns></returns>
        Task CommitAsync();
    }
}
