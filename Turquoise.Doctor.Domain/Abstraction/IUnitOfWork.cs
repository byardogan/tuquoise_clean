﻿using System.Threading.Tasks;

namespace Turquoise.Doctor.Domain.Abstraction
{
    using Turquoise.Doctor.Domain.Aggregates;

    /// <summary>
    /// UoW pattern
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Save database changes
        /// </summary>
        /// <returns></returns>
        int SaveChanges();

        /// <summary>
        /// Save database changes async
        /// </summary>
        /// <returns></returns>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// Get database transaction
        /// </summary>
        /// <returns></returns>
        ITransaction BeginTransaction();

        /// <summary>
        /// Get service from UoW
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetService<T>() where T : IRoot;
    }
}
