﻿namespace Turquoise.Doctor.Domain.Abstraction
{
    /// <summary>
    /// Visitor
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IVisitor<T>
    {
        void Visit(T target);
    }
}
