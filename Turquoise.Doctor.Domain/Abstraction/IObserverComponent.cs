﻿namespace Turquoise.Doctor.Domain.Abstraction
{
    /// <summary>
    /// Observer
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IObserverComponent<T>
    {
        void Handle(T value);
    }
}
