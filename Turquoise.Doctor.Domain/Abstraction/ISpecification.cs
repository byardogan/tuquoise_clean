﻿using System;
using System.Linq.Expressions;

namespace Turquoise.Doctor.Domain.Abstraction
{
    /// <summary>
    /// Specification
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISpecification<T>
    {
        bool IsSatisfiedBy(T arg);
        Expression<Func<T, bool>> ToExpression();
    }
}
