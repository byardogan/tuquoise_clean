﻿namespace Turquoise.Doctor.Domain.Abstraction
{
    /// <summary>
    /// Observer subject
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IObserverSubject<T>
    {
        void Notify(T value);
        IObserverSubject<T> AddObserver(IObserverComponent<T> observer);
    }
}
