﻿using System;

namespace Turquoise.Doctor.Domain.Abstraction
{
    public interface IIdentity
    {
        Guid User { get; }
    }
}
