﻿namespace Turquoise.Doctor.Domain.Abstraction
{
    public interface ISaltFactory
    {
        public byte[] Generate();
    }
}
