﻿namespace Turquoise.Doctor.Domain.Abstraction
{
    public interface IHashProvider
    {
        public byte[] Compute(string input, byte[] salt);
    }
}
