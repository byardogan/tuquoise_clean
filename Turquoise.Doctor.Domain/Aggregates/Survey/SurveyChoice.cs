﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Turquoise.Doctor.Domain.Aggregates.Survey
{
    [Table(name: "SurveyChoice", Schema = Schemas.SURVEY)]
    public class SurveyChoice : DatabaseTable
    {
        /// <summary>
        /// Survey Fk
        /// </summary>
        [ForeignKey(nameof(Survey))]
        public Guid SurveyId { get; set; }
        public Survey Survey { get; set; }

        [Column(TypeName = "text")]
        public string Option { get; set; }
        public bool? Accepted { get; set; }
    }
}
