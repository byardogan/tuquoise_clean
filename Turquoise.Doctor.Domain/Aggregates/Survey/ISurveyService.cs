﻿namespace Turquoise.Doctor.Domain.Aggregates.Survey
{
    public interface ISurveyService : ICrud<Survey>
    {
    }
}
