﻿using FluentValidation;

namespace Turquoise.Doctor.Domain.Aggregates.Survey
{
    public class ViewModelValidator : DTOValidator<SurveyViewModel>
    {
        public override void ConfigureValidations()
        {
            RuleFor(x => x.Title)
                .NotNull()
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(x => x.Body)
                .MaximumLength(1000);
        }
    }
}
