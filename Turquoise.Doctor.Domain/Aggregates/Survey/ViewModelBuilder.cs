﻿namespace Turquoise.Doctor.Domain.Aggregates.Survey
{
    public class ViewModelBuilder : DTOBuilder
    {
        public override void ConfigureMapper()
        {
            CreateMap<SurveyChoiceViewModel, SurveyChoice>();
            CreateMap<SurveyChoice, SurveyChoiceViewModel>();

            CreateMap<SurveyViewModel, Survey>();
            CreateMap<Survey, SurveyViewModel>();
        }
    }
}
