﻿namespace Turquoise.Doctor.Domain.Aggregates.Survey
{
    public class SurveyChoiceViewModel : EntityViewModel
    {
        public string Option { get; set; }
        public bool? Accepted { get; set; }
    }
}
