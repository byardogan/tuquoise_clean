﻿using System;
using System.Collections.Generic;

namespace Turquoise.Doctor.Domain.Aggregates.Survey
{
    public class SurveyViewModel : EntityViewModel
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public byte[] Image { get; set; }
        public DateTime? CreationAt { get; set; }

        /// <summary>
        /// Choices
        /// </summary>
        public virtual ICollection<SurveyChoiceViewModel> Choices { get; set; } 
            = new HashSet<SurveyChoiceViewModel>();
    }
}
