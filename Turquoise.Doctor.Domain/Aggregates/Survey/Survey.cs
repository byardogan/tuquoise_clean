﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Turquoise.Doctor.Domain.Aggregates.Survey
{
    [Table(name: "Survey", Schema = Schemas.SURVEY)]
    public class Survey : DatabaseTable, ICreationDateProperty
    {
        [Column(TypeName = "text")]
        public string Title { get; set; }

        [Column(TypeName = "text")]
        public string Body { get; set; }
        public byte[] Image { get; set; }
        public DateTime? CreationAt { get; set; }

        /// <summary>
        /// Choices
        /// </summary>
        public virtual ICollection<SurveyChoice> Choices { get; set; }
            = new HashSet<SurveyChoice>();
    }
}
