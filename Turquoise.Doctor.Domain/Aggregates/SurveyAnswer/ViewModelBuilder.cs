﻿using Turquoise.Doctor.Domain.Abstraction;

namespace Turquoise.Doctor.Domain.Aggregates.SurveyAnswer
{
    public class ViewModelBuilder : DTOBuilder
    {
        private readonly IIdentity identity;
        public ViewModelBuilder() => identity = Dependency.As<IIdentity>();

        public override void ConfigureMapper()
        {
            CreateMap<SurveyAnswer, SurveyAnswerViewModel>();
            CreateMap<SurveyAnswerViewModel, SurveyAnswer>();
        }
    }
}
