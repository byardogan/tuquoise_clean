﻿using System;

namespace Turquoise.Doctor.Domain.Aggregates.SurveyAnswer
{
    public interface ISurveyAnswerService : IRoot, ICrud<SurveyAnswer>
    {
        void UpsertChoice(Guid choice);
        void EmptyChoice(Guid choice);
    }
}
