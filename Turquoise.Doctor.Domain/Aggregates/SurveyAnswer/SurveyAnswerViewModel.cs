﻿using System;

namespace Turquoise.Doctor.Domain.Aggregates.SurveyAnswer
{
    public class SurveyAnswerViewModel : EntityViewModel
    {
        public Guid ChoiceId { get; set; }
        public string Comments { get; set; }
        public string Coordinates { get; set; }
    }
}
