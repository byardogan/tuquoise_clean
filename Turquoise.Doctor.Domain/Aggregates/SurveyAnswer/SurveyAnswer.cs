﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Turquoise.Doctor.Domain.Aggregates.SurveyAnswer
{
    using Survey;

    [Table(name: "SurveyAnswer", Schema = Schemas.SURVEY)]
    public class SurveyAnswer : DatabaseTable, ICreationDateProperty
    {
        /// <summary>
        /// Choice Fk
        /// </summary>
        [ForeignKey(nameof(Choice))]
        public Guid ChoiceId { get; set; }
        public SurveyChoice Choice { get; set; }
        public DateTime? CreationAt { get; set; }
        public string Comments { get; set; }
        public string Coordinates { get; set; }
    }
}
