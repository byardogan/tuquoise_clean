﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Turquoise.Doctor.Domain.Aggregates
{
    /// <summary>
    /// Base tabase table
    /// </summary>
    public abstract class DatabaseTable : Entity
    {
        /// <summary>
        /// Row primary key
        /// </summary>
        [
            Key,
            Column(
               name: "Id",
               Order = 0, 
               TypeName = "uuid")
        ]
        public virtual Guid Id { get; set; }
    }
}
