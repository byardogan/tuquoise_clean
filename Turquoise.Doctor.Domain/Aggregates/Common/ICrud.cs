﻿namespace Turquoise.Doctor.Domain.Aggregates
{
    /// <summary>
    /// Crud repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICrud<T> : IRoot, ICommand<T>, IQuery<T> where T : DatabaseTable
    {
    }
}
