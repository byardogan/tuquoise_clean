﻿using AutoMapper;

namespace Turquoise.Doctor.Domain.Aggregates
{
    /// <summary>
    /// Base mapper
    /// </summary>
    public abstract class DTOBuilder : Profile
    {
        public DTOBuilder()
        {
            ConfigureMapper();
        }

        /// <summary>
        /// Register mapper configuration template method
        /// </summary>
        public abstract void ConfigureMapper();
    }
}
