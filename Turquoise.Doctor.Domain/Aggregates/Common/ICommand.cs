﻿using System;

namespace Turquoise.Doctor.Domain.Aggregates
{
    /// <summary>
    /// Command respository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICommand<T> where T : DatabaseTable
    {
        T Insert(T entity);
        T Update(T entity);
        T Delete(Guid id);
    }
}
