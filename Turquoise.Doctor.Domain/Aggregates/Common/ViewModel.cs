﻿namespace Turquoise.Doctor.Domain.Aggregates
{
    using Turquoise.Doctor.Domain.Abstraction;

    /// <summary>
    /// Base domain transfer object(Dto)
    /// </summary>
    public abstract class ViewModel
    {
        /// <summary>
        /// Accept visitor
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="visitor"></param>
        /// <returns></returns>
        public virtual void Accept(IVisitor<ViewModel> visitor) => visitor.Visit(this);
    }
}
