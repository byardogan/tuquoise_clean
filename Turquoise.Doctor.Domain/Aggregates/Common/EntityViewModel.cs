﻿using System;

namespace Turquoise.Doctor.Domain.Aggregates
{
    /// <summary>
    /// Base table Dto
    /// </summary>
    public abstract class EntityViewModel : ViewModel
    {
        public virtual Guid? Id { get; set; }
    }
}
