﻿using System;

namespace Turquoise.Doctor.Domain.Aggregates
{
    public interface ICreationDateProperty
    {
        DateTime? CreationAt { get; set; }
    }
}
