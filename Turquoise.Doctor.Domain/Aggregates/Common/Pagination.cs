﻿namespace Turquoise.Doctor.Domain.Aggregates
{
    public class Pagination
    {
        public Pagination()
        {
        }

        public Pagination(int? page, int? rows)
        {
            Page = page;
            Rows = rows;
        }

        public int? Page { get; set; }
        public int? Rows { get; set; } 

        /// <summary>
        /// As tuple
        /// </summary>
        /// <param name="pagination"></param>
        public static implicit operator (int page, int rows)(Pagination pagination)
        {
            return (page: pagination.Page ?? 1, rows: pagination.Rows ?? 10);
        } 
    }
}
