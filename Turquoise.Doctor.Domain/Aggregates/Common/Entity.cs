﻿namespace Turquoise.Doctor.Domain.Aggregates
{
    using Turquoise.Doctor.Domain.Abstraction;

    /// <summary>
    /// Base database entity
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// Accept visitor
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="visitor"></param>
        /// <returns></returns>
        public virtual void Accept(IVisitor<Entity> visitor) => visitor.Visit(this);
    }
}
