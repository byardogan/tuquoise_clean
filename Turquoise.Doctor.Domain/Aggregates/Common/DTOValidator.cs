﻿using FluentValidation;

namespace Turquoise.Doctor.Domain.Aggregates
{
    /// <summary>
    /// Base DTO validator
    /// </summary>
    /// <typeparam name="TViewModel"></typeparam>
    public abstract class DTOValidator<TViewModel> : AbstractValidator<TViewModel> where TViewModel : ViewModel
    {
        public DTOValidator()
        {
            ConfigureValidations();
        }

        /// <summary>
        /// Register view model validation template method
        /// </summary>
        public abstract void ConfigureValidations();
    }
}
