﻿using System.Linq;
using System.Collections.Generic;

namespace Turquoise.Doctor.Domain.Aggregates
{
    /// <summary>
    /// Base value object
    /// </summary>
    public abstract class ValueObject
    {
        protected abstract IEnumerable<object> GetAtomicValues();

        protected static bool EqualOperator(ValueObject x, ValueObject y) => 
            !(x is null ^ y is null) && (x is null || x.Equals(y));

        protected static bool NotEqualOperator(ValueObject x, ValueObject y)
            => !EqualOperator(x, y);

        public override int GetHashCode() =>
            GetAtomicValues().Select(x => x != null ? x.GetHashCode() : 0)
                .Aggregate((x, y) => x ^ y);

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            ValueObject other = (ValueObject)obj;
            IEnumerator<object> thisValues = GetAtomicValues().GetEnumerator();
            IEnumerator<object> otherValues = other.GetAtomicValues().GetEnumerator();
            while (thisValues.MoveNext() && otherValues.MoveNext())
            {
                if (thisValues.Current is null ^ otherValues.Current is null)
                {
                    return false;
                }

                if (thisValues.Current != null && !thisValues.Current.Equals(otherValues.Current))
                {
                    return false;
                }
            }

            return !thisValues.MoveNext() && !otherValues.MoveNext();
        }
    }
}
