﻿using System;
using AutoMapper;
using System.Linq;
using System.Collections.Generic;

namespace Turquoise.Doctor.Domain.Aggregates
{
    public static class ViewModelMapper
    {
        private readonly static IMapper mapper;
        static ViewModelMapper() => mapper = Dependency.As<IMapper>();

        /// <summary>
        /// Map
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        static public T Cast<T>(this ViewModel viewModel) where T : Entity
        {
            return (viewModel is null) ? null :
                (T)mapper.Map(
                    viewModel,
                    viewModel.GetType(),
                    typeof(T));
        }

        /// <summary>
        /// Map
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        static public T Cast<T>(this Entity entity) where T : ViewModel
        {
            return (entity is null) ? null :
                (T)mapper.Map(
                    entity,
                    entity.GetType(),
                    typeof(T));
        }

        /// <summary>
        /// Map with action
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="viewModel"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        static public T Cast<T>(this ViewModel viewModel, Action<T> action) where T : Entity
        {
            if (viewModel is null)
            {
                return null;
            }

            var obj = Cast<T>(viewModel);
            action?.Invoke(obj);
            return obj;
        }

        /// <summary>
        /// Map with action
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        static public T Cast<T>(this Entity entity, Action<T> action) where T : ViewModel
        {
            if (entity is null)
            {
                return null;
            }

            var obj = Cast<T>(entity);
            action?.Invoke(obj);
            return obj;
        }

        /// <summary>
        /// Map collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="viewModels"></param>
        /// <returns></returns>
        static public T[] Cast<T>(this IEnumerable<ViewModel> viewModels) where T : Entity
        {
            return (viewModels is null) ? null :
                ((IEnumerable<T>)mapper.Map(
                    viewModels,
                    viewModels.GetType(),
                    typeof(IEnumerable<T>)))
                    .ToArray();
        }

        /// <summary>
        /// Map collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        static public T[] Cast<T>(this IEnumerable<Entity> entity) where T : ViewModel
        {
            return (entity is null) ? null :
                ((IEnumerable<T>)mapper.Map(
                    entity,
                    entity.GetType(),
                    typeof(IEnumerable<T>)))
                    .ToArray();
        }

        /// <summary>
        /// Map collection with action
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="viewModels"></param>
        /// <returns></returns>
        static public T[] Cast<T>(this IEnumerable<ViewModel> viewModels, Action<T> action) where T : Entity
        {
            if (viewModels is null)
            {
                return null;
            }

            List<T> collection = ((IEnumerable<T>)mapper.Map(
                viewModels,
                viewModels.GetType(),
                typeof(IEnumerable<T>)))
                .ToList();

            collection.ForEach(e => action?.Invoke(e));
            return collection.ToArray();
        }

        /// <summary>
        /// Map collection with action
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities"></param>
        /// <returns></returns>
        static public T[] Cast<T>(this IEnumerable<Entity> entities, Action<T> action) where T : ViewModel
        {
            if (entities is null)
            {
                return null;
            }

            List<T> collection = ((IEnumerable<T>)mapper.Map(
                entities,
                entities.GetType(),
                typeof(IEnumerable<T>)))
                .ToList();

            collection.ForEach(e => action?.Invoke(e));
            return collection.ToArray();
        }
    }
}
