﻿using System;
using MediatR;

namespace Turquoise.Doctor.Domain.Aggregates
{
    public sealed class Queries
    {
        /// <summary>
        /// Get by id
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        public class Get<TViewModel> : IRequest<TViewModel> where TViewModel : ViewModel
        {
            public Guid Identity { get; }
            public Get(Guid identity)
            {
                Identity = identity;
            }
        }

        /// <summary>
        /// Get page
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        public class GetPage<TViewModel> : IRequest<TViewModel[]> where TViewModel : ViewModel
        {
            public Pagination Pagination { get; }
            public GetPage(Pagination pagination)
            {
                Pagination = pagination;
            }
        }
    }
}
