﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Turquoise.Doctor.Domain.Aggregates
{
    public interface IUserProperty
    {
        /// <summary>
        /// User Fk
        /// </summary>
        [ForeignKey(nameof(User))]
        Guid UserId { get; set; }
        User.User User { get; set; }
    }
}
