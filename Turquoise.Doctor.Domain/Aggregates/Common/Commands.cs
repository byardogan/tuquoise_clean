﻿using System;
using MediatR;

namespace Turquoise.Doctor.Domain.Aggregates
{
    public sealed class Commands
    {
        /// <summary>
        /// Insert
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        public class Insert<TViewModel> : IRequest where TViewModel : ViewModel
        {
            public TViewModel ViewModel { get; }
            public Insert(TViewModel viewModel)
            {
                ViewModel = viewModel;
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        public class Update<TViewModel> : IRequest where TViewModel : ViewModel
        {
            public TViewModel ViewModel { get; }
            public Update(TViewModel viewModel)
            {
                ViewModel = viewModel;
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        public class Delete<TViewModel> : IRequest where TViewModel : ViewModel
        {
            public Guid Identity { get; }

            public Delete(Guid identity)
            {
                Identity = identity;
            }
        }
    }
}
