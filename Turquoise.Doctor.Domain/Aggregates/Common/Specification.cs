﻿using System;
using System.Linq.Expressions;

namespace Turquoise.Doctor.Domain.Aggregates
{
    using Turquoise.Doctor.Domain.Abstraction;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Specification<T, TFilter> : ISpecification<T>
    {
        public abstract TFilter Filters { get; }
        protected bool IsEmptyFilter { get => Filters is null; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public bool IsSatisfiedBy(T arg) => ToExpression().Compile()(arg);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public abstract Expression<Func<T, bool>> ToExpression();

        /// <summary>
        /// Merge
        /// </summary>
        /// <typeparam name="X"></typeparam>
        /// <param name="expr1"></param>
        /// <param name="expr2"></param>
        /// <returns></returns>
        public static Expression<Func<X, bool>> AndAlso<X>(Expression<Func<X, bool>> expr1, Expression<Func<X, bool>> expr2)
        {
            ParameterExpression param = expr1.Parameters[0];
            if (ReferenceEquals(param, expr2.Parameters[0]))
            {
                return Expression.Lambda<Func<X, bool>>(
                    Expression.AndAlso(expr1.Body, expr2.Body), param);
            }
            return Expression.Lambda<Func<X, bool>>(
                Expression.AndAlso(
                    expr1.Body,
                    Expression.Invoke(expr2, param)), param);
        }
    }
}
