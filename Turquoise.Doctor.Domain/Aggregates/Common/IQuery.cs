﻿using System;
using System.Linq;

namespace Turquoise.Doctor.Domain.Aggregates
{
    /// <summary>
    /// Query repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IQuery<T> where T : DatabaseTable
    {
        T GetById(Guid id);
        T[] GetByPage((int page, int rows) pagination);
        IQueryable<T> Search();
    }
}
