﻿namespace Turquoise.Doctor.Domain.Aggregates
{
    /// <summary>
    /// Schema constants
    /// </summary>
    public static class Schemas
    {
        public const string USER = "user";
        public const string SURVEY = "survey";
    }
}
