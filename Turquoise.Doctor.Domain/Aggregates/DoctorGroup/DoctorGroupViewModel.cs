﻿namespace Turquoise.Doctor.Domain.Aggregates.DoctorGroup
{
    /// <summary>
    /// Doctor groups view model
    /// </summary>
    public class DoctorGroupViewModel : EntityViewModel
    {
        public string GroupName { get; set; }
        public int MemberCount { get; set; }
    }
}
