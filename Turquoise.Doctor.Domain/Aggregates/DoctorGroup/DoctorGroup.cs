﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Turquoise.Doctor.Domain.Aggregates.DoctorGroup
{
    using User;

    /// <summary>
    /// Doctor group table
    /// </summary>
    [Table("DoctorGroup", Schema = Schemas.USER)]
    public class DoctorGroup : DatabaseTable
    {
        [Required]
        [Column(TypeName = "text")]
        public string GroupName { get; set; }

        /// <summary>
        /// Users
        /// </summary>
        public virtual ICollection<User> Users { get; set; } = new HashSet<User>();
    }
}
