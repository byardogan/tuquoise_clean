﻿using FluentValidation;

namespace Turquoise.Doctor.Domain.Aggregates.DoctorGroup
{
    /// <summary>
    /// DTO validator
    /// </summary>
    public sealed class ViewModelValidator : DTOValidator<DoctorGroupViewModel>
    {
        public override void ConfigureValidations()
        {
            RuleFor(x => x.GroupName)
                .NotNull()
                .NotEmpty()
                .MaximumLength(100);
        }
    }
}
