﻿namespace Turquoise.Doctor.Domain.Aggregates.DoctorGroup
{
    /// <summary>
    /// Mapper configuration
    /// </summary>
    public sealed class ViewModelBuilder : DTOBuilder
    {
        public override void ConfigureMapper()
        {
            CreateMap<DoctorGroup, DoctorGroupViewModel>();
            CreateMap<DoctorGroupViewModel, DoctorGroup>();
        }
    }
}
