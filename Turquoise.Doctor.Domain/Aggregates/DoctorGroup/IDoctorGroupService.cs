﻿
namespace Turquoise.Doctor.Domain.Aggregates.DoctorGroup
{
    using Turquoise.Doctor.Domain.Abstraction;
    public interface IDoctorGroupService : ICrud<DoctorGroup>
    {
    }
}
