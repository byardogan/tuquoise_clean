﻿using FluentValidation;

namespace Turquoise.Doctor.Domain.Aggregates.User
{
    /// <summary>
    /// User Dto validator
    /// </summary>
    public sealed class ViewModelValidator : AbstractValidator<UserViewModel>
    {
        public ViewModelValidator()
        {
            RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty()
                .MaximumLength(40);

            RuleFor(x => x.Surname)
                .NotNull()
                .NotEmpty()
                .MaximumLength(40);

            RuleFor(x => x.UserName)
                .NotNull()
                .NotEmpty()
                .MaximumLength(40);

            RuleFor(x => x.EmailAddress)
                .NotNull()
                .NotEmpty()
                .MaximumLength(40)
                .EmailAddress();
        }
    }
}
