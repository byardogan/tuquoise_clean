﻿using System;
using System.Linq.Expressions;

namespace Turquoise.Doctor.Domain.Aggregates.User
{
    public sealed class UserSpecification : Specification<User, UserViewModel>
    {
        public override UserViewModel Filters { get; }
        public UserSpecification(UserViewModel filters) => Filters = filters;

        public override Expression<Func<User, bool>> ToExpression()
        {
            return x => true;
        }
    }
}
