﻿namespace Turquoise.Doctor.Domain.Aggregates.User
{
    public sealed class ViewModelBuilder : DTOBuilder
    {
        public override void ConfigureMapper()
        {
            CreateMap<User, UserViewModel>();
            CreateMap<UserViewModel, User>()
                .ForMember(e => e.Group, options => options.Ignore());
        }
    }
}
