﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Turquoise.Doctor.Domain.Aggregates.User
{
    /// <summary>
    /// User password table
    /// </summary>
    [Table("UserPassword", Schema = Schemas.USER)]
    public class UserPassword : DatabaseTable
    {
        /// <summary>
        /// User Fk
        /// </summary>
        [ForeignKey(nameof(User))]
        public override Guid Id
        {
            get => base.Id;
            set => base.Id = value;
        }
        public User User { get; set; }

        [Column(TypeName = "text")]
        public string PasswordHash { get; set; }

        [Column(TypeName = "text")]
        public string PasswordSalt { get; set; }
    }
}
