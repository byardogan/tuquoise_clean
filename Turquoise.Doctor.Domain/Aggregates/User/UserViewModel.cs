﻿using System;

namespace Turquoise.Doctor.Domain.Aggregates.User
{
    using DoctorGroup;

    /// <summary>
    /// User Dto
    /// </summary>
    public class UserViewModel : EntityViewModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public DateTime? CreationAt { get; set; }
        public Guid GroupId { get; set; }
        public DoctorGroupViewModel Group { get; set; }
    }
}
