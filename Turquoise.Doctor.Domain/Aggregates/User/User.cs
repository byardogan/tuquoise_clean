﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Turquoise.Doctor.Domain.Aggregates.User
{
    using DoctorGroup;

    /// <summary>
    /// User entity
    /// </summary>
    [Table("User", Schema = Schemas.USER)]
    public class User : DatabaseTable, ICreationDateProperty
    {
        [Required]
        [Column(TypeName = "text")]
        public string Name { get; set; }

        [Required]
        [Column(TypeName = "text")]
        public string Surname { get; set; }

        [Required]
        [Column(TypeName = "text")]
        public string UserName { get; set; }

        [Required]
        [Column(TypeName = "text")]
        public string EmailAddress { get; set; }

        [Column(TypeName = "timestamp")]
        public DateTime? CreationAt { get; set; }

        /// <summary>
        /// Group Fk
        /// </summary>
        [ForeignKey(nameof(Group))]
        public Guid GroupId { get; set; }
        public DoctorGroup Group { get; set; }
    }
}
