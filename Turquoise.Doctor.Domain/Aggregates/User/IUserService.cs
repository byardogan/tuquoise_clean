﻿namespace Turquoise.Doctor.Domain.Aggregates.User
{
    using Turquoise.Doctor.Domain.Abstraction;

    /// <summary>
    /// User repository service
    /// </summary>
    public interface IUserService : ICrud<User>
    {
    }
}
