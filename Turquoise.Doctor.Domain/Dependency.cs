﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Turquoise.Doctor.Domain
{
    /// <summary>
    /// Dependency resolver
    /// </summary>
    public static class Dependency
    {
        static IServiceProvider provider;

        /// <summary>
        /// Build IoC container
        /// </summary>
        /// <param name="provider"></param>
        public static void Build(IServiceProvider provider)
        {
            if (!(Dependency.provider is null))
            {
                throw new Exception(
                     nameof(IServiceProvider) + " already created!"
                     );
            }
            Dependency.provider = provider;
        }

        /// <summary>
        /// Resolve dependency
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <returns></returns>
        public static TService As<TService>()
        {
            using IServiceScope scope = provider.CreateScope();
            return scope.ServiceProvider.GetRequiredService<TService>();
        }
    }
}
