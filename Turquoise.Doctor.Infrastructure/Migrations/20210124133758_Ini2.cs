﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Turquoise.Doctor.Infrastructure.Migrations
{
    public partial class Ini2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Coordinates",
                schema: "survey",
                table: "SurveyAnswer",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Coordinates",
                schema: "survey",
                table: "SurveyAnswer");
        }
    }
}
