﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Turquoise.Doctor.Infrastructure.Migrations
{
    public partial class Ini : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SurveyAnswer_User_UserId",
                schema: "survey",
                table: "SurveyAnswer");

            migrationBuilder.DropIndex(
                name: "IX_SurveyAnswer_UserId",
                schema: "survey",
                table: "SurveyAnswer");

            migrationBuilder.DropColumn(
                name: "UserId",
                schema: "survey",
                table: "SurveyAnswer");

            migrationBuilder.AddColumn<string>(
                name: "Comments",
                schema: "survey",
                table: "SurveyAnswer",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Comments",
                schema: "survey",
                table: "SurveyAnswer");

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                schema: "survey",
                table: "SurveyAnswer",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_SurveyAnswer_UserId",
                schema: "survey",
                table: "SurveyAnswer",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_SurveyAnswer_User_UserId",
                schema: "survey",
                table: "SurveyAnswer",
                column: "UserId",
                principalSchema: "user",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
