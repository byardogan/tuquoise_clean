﻿using System;

namespace Turquoise.Doctor.Infrastructure.Ioc
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ServiceActivator
    {
        private ServiceActivator() { }

        private static readonly Lazy<ServiceActivator> @object;

        /// <summary>
        /// Prepares to lazy object
        /// </summary>
        static ServiceActivator() => @object = new Lazy<ServiceActivator>(
                () => new ServiceActivator(), true);

        public static ServiceActivator Container() => @object.Value;
    }
}
