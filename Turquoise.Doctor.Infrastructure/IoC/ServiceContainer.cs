﻿using Microsoft.Extensions.DependencyInjection;

namespace Turquoise.Doctor.Infrastructure.Ioc
{
    using Turquoise.Doctor.Domain.Abstraction;
    using Turquoise.Doctor.Infrastructure.Persistence;

    /// <summary>
    /// Dependency injection container
    /// </summary>
    public partial class ServiceActivator
    {
        public void Register(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
