﻿using System;
using System.Collections.Generic;

namespace Turquoise.Doctor.Infrastructure.Persistence
{
    using Turquoise.Doctor.Domain.Aggregates.User;
    using Turquoise.Doctor.Domain.Aggregates.Survey;
    using Turquoise.Doctor.Domain.Aggregates.DoctorGroup;
    using Turquoise.Doctor.Infrastructure.Persistence.Service;
    using Turquoise.Doctor.Domain.Aggregates.SurveyAnswer;

    public static class ServiceLocator
    {
        static ServiceLocator()
        {
            //register:
            static void Register<TImplemetation, TService>() => services.Add(typeof(TImplemetation), typeof(TService));

            Register<IUserService, UserService>();
            Register<ISurveyService, SurveyService>();
            Register<IDoctorGroupService, DoctorGroupService>();
            Register<ISurveyAnswerService, SurveyAnswerService>();
        }

        private static readonly Dictionary<Type, Type> services = new Dictionary<Type, Type>();

        /// <summary>
        /// Resolve type
        /// </summary>
        /// <typeparam name="TInterface"></typeparam>
        /// <returns></returns>
        public static Type Resolve<TInterface>()
        {
            if (!services.TryGetValue(typeof(TInterface), out Type service))
            {
                throw new Exception("Some services didnot implement!");
            }
            return service;
        }
    }
}
