﻿using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Turquoise.Doctor.Infrastructure.Persistence
{
    using Turquoise.Doctor.Domain.Aggregates.User;
    using Turquoise.Doctor.Infrastructure.Persistence.Builder;
    using Turquoise.Doctor.Infrastructure.Persistence.Observer;
    public partial class TurquoiseDb : DbContext
    {
        protected TurquoiseDb()
        {
        }

        public TurquoiseDb([NotNull] DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// Build entities
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasPostgresExtension("uuid-ossp");
            builder.Dates();
            builder.Identities();

            builder.Entity<User>().HasIndex(x => x.UserName).IsUnique();
            builder.Entity<User>().HasIndex(x => x.EmailAddress).IsUnique();

            base.OnModelCreating(builder);
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {
            ObservableTracking();
            return base.SaveChanges();
        }

        /// <summary>
        /// Save async
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            ObservableTracking();
            return base.SaveChangesAsync(cancellationToken);
        }

        /// <summary>
        /// Obsever
        /// </summary>
        private void ObservableTracking()
        {
            ChangeTracker.DetectChanges();
            new DatabaseSubject().AddObserver(new InsertObserver())
                .AddObserver(new UpdateObserver())
                      .Notify(ChangeTracker.Entries());
        }
    }
}
