﻿using System.Threading.Tasks;
using static System.Activator;

namespace Turquoise.Doctor.Infrastructure.Persistence
{
    using Turquoise.Doctor.Domain.Aggregates;
    using Turquoise.Doctor.Domain.Abstraction;

    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly TurquoiseDb context;
        public UnitOfWork(TurquoiseDb context) => this.context = context;

        /// <summary>
        /// Begin new transaction
        /// </summary>
        /// <returns></returns>
        public ITransaction BeginTransaction()
        {
            return new TransactionScope(context);
        }

        /// <summary>
        /// Get service
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetService<T>() where T : IRoot
        {
            var service = ServiceLocator.Resolve<T>();
            return (T)CreateInstance(service, new object[] { context });
        }

        /// <summary>
        /// Save changes
        /// </summary>
        /// <returns></returns>
        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        /// <summary>
        /// Save changes
        /// </summary>
        /// <returns></returns>
        public Task<int> SaveChangesAsync()
        {
            return context.SaveChangesAsync();
        }
    }
}
