﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace Turquoise.Doctor.Infrastructure.Persistence
{
    using Turquoise.Doctor.Domain.Abstraction;
    public sealed class TransactionScope : ITransaction
    {
        private readonly IDbContextTransaction transaction;
        public TransactionScope(TurquoiseDb context) => transaction = context.Database.BeginTransaction();

        /// <summary>
        /// Commit
        /// </summary>
        /// <returns></returns>
        public Task CommitAsync() => transaction.CommitAsync();

        /// <summary>
        /// Rollback
        /// </summary>
        /// <returns></returns>
        public Task RollbackAsync() => transaction.RollbackAsync();

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose() => transaction.DisposeAsync();
    }
}
