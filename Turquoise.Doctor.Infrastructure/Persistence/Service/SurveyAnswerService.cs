﻿using System;

namespace Turquoise.Doctor.Infrastructure.Persistence.Service
{
    using Turquoise.Doctor.Domain.Aggregates.SurveyAnswer;

    public class SurveyAnswerService : PersistentService<SurveyAnswer>, ISurveyAnswerService
    {
        public SurveyAnswerService(TurquoiseDb database) : base(database)
        {
        }

        /// <summary>
        /// Add or update choice
        /// </summary>
        /// <param name="choice"></param>
        public void UpsertChoice(Guid choice)
        {
        }

        /// <summary>
        /// Remove choice
        /// </summary>
        /// <param name="choice"></param>
        public void EmptyChoice(Guid choice)
        {
        }
    }
}
