﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Turquoise.Doctor.Infrastructure.Persistence.Service
{
    using Turquoise.Doctor.Domain.Aggregates.Survey;
    public class SurveyService : PersistentService<Survey>, ISurveyService
    {
        public SurveyService(TurquoiseDb database) : base(database)
        {
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override Survey GetById(Guid id) => Service.AsNoTracking()
            .Include(e => e.Choices)
            .SingleOrDefault(e => e.Id == id);
    }
}
