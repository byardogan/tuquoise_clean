﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Turquoise.Doctor.Infrastructure.Persistence.Service
{
    using Turquoise.Doctor.Domain.Aggregates.DoctorGroup;

    public class DoctorGroupService : PersistentService<DoctorGroup>, IDoctorGroupService
    {
        public DoctorGroupService(TurquoiseDb database) : base(database)
        {
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override DoctorGroup GetById(Guid id) => Service
            .Include(e => e.Users)
            .AsNoTracking()
            .SingleOrDefault(e => e.Id == id);
    }
}
