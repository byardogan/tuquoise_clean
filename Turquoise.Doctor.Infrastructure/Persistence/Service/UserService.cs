﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Turquoise.Doctor.Infrastructure.Persistence.Service
{
    using Turquoise.Doctor.Domain.Aggregates.User;
    public class UserService : PersistentService<User>, IUserService
    {
        public UserService(TurquoiseDb database) : base(database)
        {
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override User GetById(Guid id) => Service.AsNoTracking()
            .Include(e => e.Group)
            .SingleOrDefault(e => e.Id == id);

        /// <summary>
        /// Get by page
        /// </summary>
        /// <param name="pagination"></param>
        /// <returns></returns>
        public override User[] GetByPage((int page, int rows) pagination) => Service.AsNoTracking()
            .Include(e => e.Group)
            .Skip((pagination.page - 1) * pagination.rows)
            .Take(pagination.rows)
            .ToArray();
    }
}
