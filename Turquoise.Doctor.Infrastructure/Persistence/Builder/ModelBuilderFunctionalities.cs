﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Turquoise.Doctor.Infrastructure.Persistence.Builder
{
    using Turquoise.Doctor.Domain.Aggregates;
    public static class ModelBuilderFunctionalities
    {
        public static void Dates(this ModelBuilder builder)
        {
            var dates = builder.Model.GetEntityTypes()
                .Where(e => e.ClrType.GetInterfaces().Contains(typeof(ICreationDateProperty)))
                .Select(e => e.ClrType);

            foreach (Type type in dates)
            {
                builder.Entity(type).Property(nameof(ICreationDateProperty.CreationAt))
                    .HasDefaultValueSql("now()");
            }
        }

        public static void Identities(this ModelBuilder builder)
        {
            var identities = builder.Model.GetEntityTypes()
                .Where(e => e.ClrType.BaseType == typeof(DatabaseTable))
                .Select(e => e.ClrType);

            foreach (Type type in identities)
            {
                builder.Entity(type).Property(nameof(DatabaseTable.Id))
                    .HasDefaultValueSql("uuid_generate_v4()");
            }
        }
    }
}
