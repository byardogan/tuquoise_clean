﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Turquoise.Doctor.Infrastructure.Persistence
{
    using Turquoise.Doctor.Domain.Aggregates;
    public abstract class PersistentService<TEntity> : IQuery<TEntity>, ICommand<TEntity> where TEntity : DatabaseTable
    {
        public PersistentService(TurquoiseDb database)
        {
            Database = database;
            Service = database.Set<TEntity>();
        }

        /// <summary>
        /// Database
        /// </summary>
        public TurquoiseDb Database { get; }

        /// <summary>
        /// Db set
        /// </summary>
        public DbSet<TEntity> Service { get; }


        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TEntity Delete(Guid id)
        {
            var entity = Service.Single(e => e.Id == id);
            Service.Remove(entity);
            return entity;
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TEntity GetById(Guid id) => Service.AsNoTracking()
            .SingleOrDefault(e => e.Id == id);

        /// <summary>
        /// Get by page
        /// </summary>
        /// <param name="pagination"></param>
        /// <returns></returns>
        public virtual TEntity[] GetByPage((int page, int rows) pagination) => Service.AsNoTracking()
            .Skip((pagination.page - 1) * pagination.rows)
            .Take(pagination.rows)
            .ToArray();

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual TEntity Insert(TEntity entity)
        {
            Service.Add(entity);
            return entity;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual TEntity Update(TEntity entity)
        {
            Database.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <returns></returns>
        public IQueryable<TEntity> Search() => Service.AsNoTracking();
    }
}
