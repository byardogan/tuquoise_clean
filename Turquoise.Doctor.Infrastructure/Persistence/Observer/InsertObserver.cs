﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Turquoise.Doctor.Infrastructure.Persistence.Observer
{
    using Turquoise.Doctor.Domain;
    using Turquoise.Doctor.Domain.Abstraction;
    using Turquoise.Doctor.Domain.Aggregates;
    public class InsertObserver : IObserverComponent<IEnumerable<EntityEntry>>
    {
        public void Handle(IEnumerable<EntityEntry> value)
        {
            IEnumerable<EntityEntry> addeds = value.Where(x => x.State == EntityState.Added);
            if (addeds.Any())
            {
                IIdentity identity = Dependency.As<IIdentity>();
                foreach (var entry in addeds)
                {
                    object entity = entry.Entity;
                    if (entity is IUserProperty userProperty)
                    {
                        userProperty.UserId = identity.User;
                    }

                    if (entity is ICreationDateProperty dateProperty)
                    {
                        dateProperty.CreationAt = null;
                    }
                }
            }
        }
    }
}
