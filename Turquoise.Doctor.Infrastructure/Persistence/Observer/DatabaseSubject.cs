﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Turquoise.Doctor.Infrastructure.Persistence.Observer
{
    using Turquoise.Doctor.Domain.Abstraction;
    public sealed class DatabaseSubject : IObserverSubject<IEnumerable<EntityEntry>>
    {
        private readonly ICollection<IObserverComponent<IEnumerable<EntityEntry>>> observers =
            new HashSet<IObserverComponent<IEnumerable<EntityEntry>>>();

        /// <summary>
        /// Add observer
        /// </summary>
        /// <param name="observer"></param>
        /// <returns></returns>
        public IObserverSubject<IEnumerable<EntityEntry>> AddObserver(IObserverComponent<IEnumerable<EntityEntry>> observer)
        {
            observers.Add(observer);
            return this;
        }

        /// <summary>
        /// Notify to observers
        /// </summary>
        /// <param name="value"></param>
        public void Notify(IEnumerable<EntityEntry> value)
        {
            foreach (IObserverComponent<IEnumerable<EntityEntry>> observer in observers)
            {
                observer.Handle(value);
            }
        }
    }
}
