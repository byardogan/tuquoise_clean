﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Turquoise.Doctor.Infrastructure.Persistence.Observer
{
    using Turquoise.Doctor.Domain.Abstraction;
    using Turquoise.Doctor.Domain.Aggregates;
    public class UpdateObserver: IObserverComponent<IEnumerable<EntityEntry>>
    {
        public void Handle(IEnumerable<EntityEntry> value)
        {
            IEnumerable<EntityEntry> updateds = value.Where(x => x.State == EntityState.Modified);
            if (updateds.Any())
            {
                foreach (var entry in updateds)
                {
                    object entity = entry.Entity;
                    entry.Property(nameof(DatabaseTable.Id)).IsModified = false;

                    if (entity is IUserProperty)
                    {
                        entry.Property(nameof(IUserProperty.UserId)).IsModified = false;
                    }

                    if (entity is ICreationDateProperty)
                    {
                        entry.Property(nameof(ICreationDateProperty.CreationAt)).IsModified = false;
                    }
                }
            }
        }
    }
}
