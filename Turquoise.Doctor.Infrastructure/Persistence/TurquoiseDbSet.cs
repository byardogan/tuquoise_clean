﻿using Microsoft.EntityFrameworkCore;

namespace Turquoise.Doctor.Infrastructure.Persistence
{
    using Turquoise.Doctor.Domain.Aggregates.User;
    using Turquoise.Doctor.Domain.Aggregates.Survey;
    using Turquoise.Doctor.Domain.Aggregates.SurveyAnswer;
    using Turquoise.Doctor.Domain.Aggregates.DoctorGroup;

    public partial class TurquoiseDb
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserPassword> UserPasswords { get; set; }
        public DbSet<DoctorGroup> DoctorGroups { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<SurveyChoice> SurveyChoices { get; set; }
        public DbSet<SurveyAnswer> SurveyAnswers { get; set; }
    }
}
